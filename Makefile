
# Docs here: https://linux.die.net/man/1/gpg2
# and OpenSSL, here: https://www.zimuel.it/blog/sign-and-verify-a-file-using-openssl

# Servers that worked:
# http://pool.sks-keyservers.net/

# On Mac, run this:
# brew install gnupg2

OSSL=/usr/local/Cellar/openssl\@1.1/1.1.1o/bin/openssl

GPG=gpg # --batch

install: ;
	$(info see here: brew install gnupg2, or here: http://macappstore.org/gnupg2/)

verify_pass:
	$(GPG) --export-secret-keys -a $(KEYID) > /dev/null && echo OK

list:
	$(GPG) --list-secret-keys --keyid-format LONG

exp:
	$(GPG) --armor --export $(KEYID)

new:
	$(GPG) --full-generate-key
	
publish:
	$(GPG) --send-keys $(KEYID)

# make FILE=Makefile DEFAULT_KEY_NAME=caca123 sign
# make FILE=Makefile DEFAULT_KEY_NAME=E30925CD2A38FA16708A95E81D34C3E3876648EF sign
sign:
	$(GPG) --default-key $(DEFAULT_KEY_NAME) --detach-sign -a $(FILE)

# make verify SIG=Makefile.asc FILE=Makefile
verify:
	$(GPG) --keyserver http://pool.sks-keyservers.net/ --verify $(SIG) $(FILE)

ossl.sign:
	$(OSSL) dgst -sha256 -sign $(PRIVATE_KEY) $(FILE) | $ossl base64 | tee $(SIG_FILE)

ossl.verify:
	$(OSSL) dgst -sha256 -verify $(PUBLIC_KEY) -signature $(SIG_FILE) $(FILE)

ossl.getpubkey:
	$(OSSL) rsa -in $(PRIVATE_KEY) -pubout -out $(PUBLIC_KEY)
